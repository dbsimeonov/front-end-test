/**
 * The internal dependencies.
 */
import Vue from 'vue'
import Router from 'vue-router'

/**
 * The external dependencies.
 */
import Home from './views/Home.vue'
import Team from './views/Team.vue'
import User from './views/User.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/team',
      name: 'Team',
      component: Team
    },
    {
      path: '/team/:userId',
      name: 'User',
      component: User,
      props: true
    }
  ]
})
