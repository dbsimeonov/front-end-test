/**
 * The internal dependencies.
 */
import Vue from 'vue'
import Vuex from 'vuex'

/**
 * The external dependencies.
 */
import axios from 'axios'

const REQUEST_TEAM_SUCCESS = 'request/team-success'
const REQUEST_TEAM_FAIL = 'request/team-fail'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    team: null,
    reqError: null
  },
  mutations: {
    [REQUEST_TEAM_SUCCESS]: (state, payload) => {
      state.team = payload
    },
    [REQUEST_TEAM_FAIL]: (state, payload) => {
      state.reqError = payload
    }
  },
  actions: {
    requestTeam({commit}){
      axios.get('/db/data.json')
        .then( res => {
          commit(REQUEST_TEAM_SUCCESS, res.data)
        })
        .catch(err => {
          commit(REQUEST_TEAM_FAIL, err)
        })
    }
  },
  getters: {
    getTeam: state => state.team,
    getUser: state => id => state.team.find(usr => usr.lastName === id)
  }
})