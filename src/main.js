/**
 * The internal dependencies.
 */
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

/**
 * The supported styles.
 */
import 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css';

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
  created() {
    this.$store.dispatch('requestTeam')
  }
}).$mount('#app')
